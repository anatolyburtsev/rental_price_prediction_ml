import json
import logging

import constants
import requests


class ZumperAPIException(Exception):
    """Custom exception class for handling Zumper API errors."""


def get_xz_token() -> str:
    """
    Fetch the xz token from Zumper API.

    Raises:
        ZumperAPIException: If the response status is not OK or the xz token is not
        found.

    Returns:
        str: The fetched xz token.
    """
    resp = requests.get(constants.XZ_TOKEN_URL, timeout=constants.REQUEST_TIMEOUT)
    if not resp.ok:
        raise ZumperAPIException(
            f"Failed to get xz token. code: {resp.status_code}, response: {resp.text}"
        )
    return json.loads(resp.text)["xz_token"]


def _get_xz_headers(xz_token) -> object:
    """
    Helper function to create xz headers for Zumper API requests.

    Args:
        xz_token (str): The xz token.

    Returns:
        dict: Headers with xz token.
    """
    xz_token = get_xz_token() if xz_token is None else xz_token
    return {
        "X-Zumper-XZ-Token": xz_token,
    }


def _safe_get_key(object_, key):
    """
    Helper function to safely get a key from an object.

    Args:
        object_ (dict): The object to get the key from.
        key (str): The key to get.

    Raises:
        ZumperAPIException: If the key is not found in the object.

    Returns:
        The value of the key in the object.
    """
    if key not in object_:
        raise ZumperAPIException(f"key: {key} not found in object: {object_}")
    return object_[key]


def _safe_parse_json(json_str):
    """
    Helper function to safely parse a JSON string.

    Args:
        json_str (str): The JSON string to parse.

    Raises:
        Exception: If the JSON string cannot be parsed.

    Returns:
        dict: The parsed JSON object.
    """
    try:
        return json.loads(json_str)
    except Exception as error:
        logging.error(f"Failed to parse json: {json_str}")
        raise error


def get_all_listings(url, xz_token=None, one_page_only=False):
    """
    Fetch all listings from the Zumper API.

    Args:
        url (str): The URL of the city to fetch listings for.
        xz_token (str, optional): The xz token for the API request. Defaults to None.
        one_page_only (bool, optional): Whether to fetch only one page of data.
        Defaults to False.

    Returns:
        list: A list of all listings.
    """
    headers = _get_xz_headers(xz_token=xz_token)
    listing_params = {**constants.DEFAULT_LISTING_REQUEST_PARAMS, "url": url}
    raw_data = requests.post(
        constants.LISTING_URL,
        headers=headers,
        json=listing_params,
        timeout=constants.REQUEST_TIMEOUT,
    )
    parsed_data = _safe_parse_json(raw_data.text)
    data = _safe_get_key(parsed_data, "listables")
    expected_total = _safe_get_key(parsed_data, "matching")
    logging.debug(f"Fetch first batch of data. Expected {expected_total} records")
    exclude_group_ids = []
    while len(exclude_group_ids) <= expected_total and not one_page_only:
        exclude_group_ids = [x["group_id"] for x in data]
        batch_raw = requests.post(
            constants.LISTING_URL,
            headers=headers,
            json={**listing_params, "excludeGroupIds": str(exclude_group_ids)},
            timeout=constants.REQUEST_TIMEOUT,
        )

        batch = _safe_parse_json(batch_raw.text)["listables"]
        logging.debug(f"got {len(batch)} records")
        if len(batch) == 0:
            break
        data += batch
    return data
