import json
import logging
from datetime import datetime
from os import path

import awswrangler as wr
import pandas as pd


def _json_to_df(data) -> pd.DataFrame:
    data_json = json.dumps(data)
    df = pd.read_json(data_json)
    return df


def _get_filename(prefix: str, date: datetime) -> str:
    return f"{prefix}_{date.strftime('%Y-%m-%d-%H-%M')}.csv"


def store_json_to_csv_local_file(
    data, filename_prefix="output", today=datetime.now()
) -> str:
    """
    Store JSON data into a local CSV file.

    :param data: JSON data to be converted and stored as a CSV file.
    :type data: List[Dict]
    :param filename_prefix: Prefix for the output CSV file. Default is 'output'.
    :type filename_prefix: str
    :param today: Date used for generating the filename. Default is the current date.
    :type today: datetime
    """
    df = _json_to_df(data)
    filename = _get_filename(filename_prefix, today)
    output_filename = path.abspath(filename)
    df.to_csv(output_filename, index=False)  # pylint: disable=no-member
    return output_filename


def store_json_to_csv_s3(data, s3_path, filename_prefix="output", today=datetime.now()):
    """
    Store JSON data into a CSV file in an Amazon S3 bucket.

    :param data: JSON data to be converted and stored as a CSV file.
    :type data: List[Dict]
    :param s3_path: Path to the S3 bucket where the CSV file will be stored.
    :type s3_path: str
    :param filename_prefix: Prefix for the output CSV file. Default is 'output'.
    :type filename_prefix: str
    :param today: Date used for generating the S3 object key. Default is the
    current date.
    :type today: datetime
    """
    df = _json_to_df(data)
    wr.s3.to_csv(
        df=df,
        path=f"{s3_path}/year={today.year}/month={today.month}/day={today.day}"
        f"/hour={today.hour}/{filename_prefix}.csv",
    )


def dummy_store_data(data):
    """
    Log the size of the input data and an example of a single line.

    :param data: Input data for logging.
    :type data: List[Dict]
    """
    logging.info(f"data size is {len(data)}. Example of one line: {data[0]}")
