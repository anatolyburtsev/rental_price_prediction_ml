import click
import io_lib
import zumper_lib


@click.command()
@click.option(
    "--city",
    type=click.STRING,
    default="vancouver-bc",
    help="The city to fetch listings for",
)
@click.option(
    "--output_path",
    type=click.Path(exists=False),
    default="./",
    help="The path to store the csv file in",
)
def fetch_data(city: str, output_path: str):
    """Fetch data from the Zumper API"""
    data = zumper_lib.get_all_listings(city)
    output_file = io_lib.store_json_to_csv_local_file(data, output_path + city)
    print(f"Data stored to {output_file}")


if __name__ == "__main__":
    fetch_data()  # pylint: disable=no-value-for-parameter
