from os import path

import click
import numpy as np
import pandas as pd

MIN_PRICE = 1000
MAX_PRICE_PERCENTILE = 96
MAX_BEDROOMS = 5


@click.command()
@click.option(
    "--input_file",
    type=click.Path(exists=True),
    help="path to file containing the raw data",
)
@click.option(
    "--output_path",
    type=click.Path(exists=False),
    default="data/interim",
    help="The path to store the cleaned csv file in",
)
def clean_data(input_file: str, output_path: str) -> None:
    """
    Clean the data.
    """
    columns_to_drop = [
        "listing_status",
        "brokerage_homepage",
        "viewings",
        "pl_url",
        "external",
        "amenity_json",
        "rating",
        "is_filter_ad",
        "max_children",
        "max_adults",
        "pet_policy_json",
    ]

    raw_df = pd.read_csv(input_file, index_col="listing_id").drop(
        columns=columns_to_drop
    )

    # flatten combined records
    df_single_appart = (
        raw_df.query("min_bedrooms == max_bedrooms")
        .rename(
            columns={
                "min_price": "price",
                "min_bedrooms": "bedrooms",
                "min_bathrooms": "bathrooms",
                "min_all_bathrooms": "all_bathrooms",
            }
        )
        .drop(
            columns=["max_price", "max_bedrooms", "max_bathrooms", "max_all_bathrooms"]
        )
    )

    df_multiple_appart = raw_df.query("min_bedrooms != max_bedrooms")
    df_max_rooms = (
        df_multiple_appart.copy()
        .rename(
            columns={
                "max_price": "price",
                "max_bedrooms": "bedrooms",
                "max_bathrooms": "bathrooms",
                "max_all_bathrooms": "all_bathrooms",
            }
        )
        .drop(
            columns=["min_price", "min_bedrooms", "min_bathrooms", "min_all_bathrooms"]
        )
    )

    df_min_rooms = (
        df_multiple_appart.copy()
        .rename(
            columns={
                "min_price": "price",
                "min_bedrooms": "bedrooms",
                "min_bathrooms": "bathrooms",
                "min_all_bathrooms": "all_bathrooms",
            }
        )
        .drop(
            columns=["max_price", "max_bedrooms", "max_bathrooms", "max_all_bathrooms"]
        )
    )

    df_flattened = pd.concat([df_single_appart, df_max_rooms, df_min_rooms])
    max_price = np.percentile(  # noqa: F841 # pylint: disable=W0612
        df_flattened["price"], MAX_PRICE_PERCENTILE
    )
    df_no_outliers = df_flattened.query(
        "price >= @MIN_PRICE and price <= @max_price and bedrooms <= @MAX_BEDROOMS"
    )

    input_filename, _ = path.splitext(path.basename(input_file))
    output_filename = path.join(output_path, f"{input_filename}_cleaned.csv")
    df_no_outliers.to_csv(output_filename)
    print(f"Saved cleaned data to {output_filename}")


if __name__ == "__main__":
    clean_data()  # pylint: disable=no-value-for-parameter
