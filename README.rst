==========================
rental_price_prediction_ml
==========================



.. image:: https://img.shields.io/pypi/v/rental_price_prediction_ml.svg
        :target: https://pypi.python.org/pypi/rental_price_prediction_ml

.. image:: https://img.shields.io/travis/anatolyburtsev/rental_price_prediction_ml.svg
        :target: https://travis-ci.com/anatolyburtsev/rental_price_prediction_ml

.. image:: https://readthedocs.org/projects/rental-price-prediction-ml/badge/?version=latest
        :target: https://rental-price-prediction-ml.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/anatolyburtsev/rental_price_prediction_ml/shield.svg
     :target: https://pyup.io/repos/github/anatolyburtsev/rental_price_prediction_ml/
     :alt: Updates



rental_price_prediction_ml


* Free software: MIT license
* Documentation: https://rental-price-prediction-ml.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
