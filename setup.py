#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = [ ]

setup(
    author="Anatolii Burtsev",
    author_email='ab.canada.world@gmail.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="rental_price_prediction_ml",
    entry_points={
        'console_scripts': [
            'rental_price_prediction_ml=rental_price_prediction_ml.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='rental_price_prediction_ml',
    name='rental_price_prediction_ml',
    packages=find_packages(include=['rental_price_prediction_ml', 'rental_price_prediction_ml.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/anatolyburtsev/rental_price_prediction_ml',
    version='0.1.0',
    zip_safe=False,
)
